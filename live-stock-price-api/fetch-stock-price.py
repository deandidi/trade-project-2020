import datetime
import logging

import pandas as pd
import pandas_datareader as pdr
from flask import Flask, Response
from flask_restplus import Api, Resource, fields
from flask_cors import CORS

LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

stock = api.model('stock', {
    'ticker': fields.String(required=True, description='Ticker'),
})


def toDate(dateString):
    return datetime.datetime.strptime(dateString, "%Y-%m-%d").date()


@api.route('/price/<ticker>', defaults={"startDate": "2020-01-01", "endDate": str(datetime.datetime.date(datetime.datetime.now()))})
@api.route('/price/<ticker>&<startDate>', defaults={"endDate": str(datetime.datetime.date(datetime.datetime.now()))})
@api.route('/price/<ticker>&<startDate>&<endDate>')
@api.param('ticker', 'The stock ticker to get a price for')
@api.param('startDate', ' The date from which to track the stock')
@api.param('endDate', ' The date till which to track the stock')
class Price(Resource):
    def get(self, ticker, startDate, endDate):
        print(startDate)
        print()
        start_date = toDate(startDate)  # datetime.date(2020, 2, 21)
        end_date = toDate(endDate)
        # start_date = datetime.datetime.now() - datetime.timedelta(1)
        df_list = list()
        try:
            for stock in ticker.split(","):
                stock_df = pdr.get_data_yahoo(
                    stock, start_date, end_date)
                stock_df["ticker"] = stock
                df_list.append(stock_df)

        except Exception as ex:
            LOG.error('Error getting data: ' + str(ex))

        df = pd.concat(df_list)

        LOG.error('Recieved price data:')
        LOG.error(str(df))

        df['Date'] = df.index

        df = df[['ticker', 'Date', 'Open', 'High', 'Close', 'Adj Close']]
        df["Date"] = df["Date"].apply(lambda x: x.strftime("%m/%d/%Y"))

        return Response(df.to_json(orient='records'), mimetype='application/json')


if __name__ == '__main__':
    app.run(debug=True)
