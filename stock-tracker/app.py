import datetime

import pandas as pd
import requests
from slack import WebClient

pd.options.plotting.backend = "plotly"


def AlertFinder(data):
    # in this function we find if stock is below certain moving average
    # if yes, we will trigger alert for this stock
    ma = data['Close'].rolling(window=30).mean()
    data['MA30'] = ma

    if ma.iloc[[-1]].values[0] < data.iloc[[-1]]['Close'].values[0]:
        # fig = plt.figure()
        # data.set_index('Date',inplace=True)
        fig = data.set_index('Date').iloc[31:][['Close', 'MA30']].plot.line(
            title="Moving Average (30 days) and Closing price for " + data.iloc[-1]["ticker"])
        fig.write_html("plots/"+data.iloc[-1]["ticker"]+".html")
        return True
    return False


def main():
    s = requests.get("http://localhost:8090/trades")
    my_json = s.content.decode('utf8').replace("'", '"')
    s = pd.read_json(my_json)
    stocks = s['ticker'].unique().tolist()

    # Create a slack client
    slack_web_client = WebClient(
        token="xoxb-1365526430435-1350587706599-8yYvq5m84Fyh6cXMlotI72XZ")

    # select start 100 days ago
    startDate = str(datetime.datetime.date(
        datetime.datetime.now() - datetime.timedelta(days=100)))

    for s in stocks:
        r = requests.get("http://127.0.0.1:5000/price/"+s+"&"+startDate)
        my_json = r.content.decode('utf8').replace("'", '"')
        df = pd.read_json(my_json)
        buy = AlertFinder(df)
        if buy:
            df = df.tail(1)

            # Create and send message
            message = {
                "channel": "#project",
                "text": "## You may want to book your profit on stock trading higher then their 30 days moving average",
                "attachments": [{"text": "ticker: " + df.iloc[-1]["ticker"] + "\nDate: " + str(df.iloc[-1]['Date']) + "\nOpen: " + str(df.iloc[-1]["Open"]) + "\nClose: " + str(df.iloc[-1]["Close"])}],
            }
            slack_web_client.chat_postMessage(**message)

            # Send Stock plot html files
            attach = {
                "channels": "#project",
                "file": "plots/"+df.iloc[-1]["ticker"]+".html"
            }
            slack_web_client.files_upload(**attach)


if __name__ == "__main__":
    main()
