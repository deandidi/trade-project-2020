package com.conygre.finance.financeapp.controllers;

import java.util.Collection;

import com.conygre.finance.financeapp.entities.Holding;
import com.conygre.finance.financeapp.entities.Holding.HoldingType;
import com.conygre.finance.financeapp.service.PortfolioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/holdings")
public class PortfolioController {
    @Autowired
    private PortfolioService portfolioService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Holding> getHoldings() {
        return portfolioService.getHoldings();
    }

    @RequestMapping(value = "/{type}", method = RequestMethod.GET)
    public Collection<Holding> getHoldings(@PathVariable("type") String type) {
        try {
            HoldingType holdingType = HoldingType.valueOf(type.toUpperCase());
            return portfolioService.getHoldings(holdingType);
        } catch (Exception e) {
            System.out.println("invalid holding type" + type);
            return null;
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> addHolding(@RequestBody Holding holding) {
        portfolioService.addHolding(holding);
        return new ResponseEntity<>("Holding added", HttpStatus.OK);
    }

}
