package com.conygre.finance.financeapp.entities;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {

    public enum TradeState {
        CREATED("CREATED"),
        PROCESSING("PROCESSING"),
        FILLED("FILLED"),
        REJECTED("REJECTED");
    
        private String state;
    
        private TradeState(String state) {
            this.state = state;
        }
    
        public String getState() {
            return this.state;
        } 
    }
    
    @Id
    private ObjectId id;
    private Date date;
    private String ticker;
    private double quantity;
    private double requestedPrice;
    private TradeState tradeState;

    public boolean verify() {
        return !(id == null || date == null || ticker == null || quantity == 0 || requestedPrice == 0 || tradeState == null);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getRequestedPrice() {
        return requestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public TradeState getTradeState() {
        return tradeState;
    }

    public void setTradeState(TradeState tradeState) {
        this.tradeState = tradeState;
    }

}