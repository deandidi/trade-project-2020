package com.conygre.finance.financeapp.service;

import java.util.Collection;

import com.conygre.finance.financeapp.entities.Holding;
import com.conygre.finance.financeapp.entities.Holding.HoldingType;

import org.bson.types.ObjectId;

public interface PortfolioService {
    Collection<Holding> getHoldings();
    Collection<Holding> getHoldings(HoldingType holdingType);
    double getBalance();
    void updateBalance(double newBalance);
    void addHolding(Holding holding);
    void removeHolding(ObjectId id);
}
