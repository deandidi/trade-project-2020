package com.conygre.finance.financeapp.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Holding {
    public enum HoldingType {
        CASH("CASH"), STOCKS("STOCKS"), BONDS("BONDS");

        private String type;

        private HoldingType(String type) {
            this.type = type;
        }

        public String getHoldingType() {
            return this.type;
        }
    }

    @Id
    private ObjectId id;
    private HoldingType holdingType;
    private String name;
    private double price;
    private double amount;

    public HoldingType getHoldingType() {
        return holdingType;
    }

    public void setHoldingType(HoldingType holdingType) {
        this.holdingType = holdingType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
