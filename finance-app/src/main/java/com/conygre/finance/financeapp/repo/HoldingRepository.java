package com.conygre.finance.financeapp.repo;

import java.util.Collection;

import com.conygre.finance.financeapp.entities.Holding;
import com.conygre.finance.financeapp.entities.Holding.HoldingType;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HoldingRepository extends MongoRepository<Holding, ObjectId> {
    public Collection<Holding> findByHoldingType(HoldingType holdingType);
}
