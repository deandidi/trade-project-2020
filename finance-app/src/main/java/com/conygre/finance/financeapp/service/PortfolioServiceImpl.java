package com.conygre.finance.financeapp.service;

import java.util.Collection;

import com.conygre.finance.financeapp.entities.Holding;
import com.conygre.finance.financeapp.entities.Holding.HoldingType;
import com.conygre.finance.financeapp.repo.HoldingRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PortfolioServiceImpl implements PortfolioService {

    @Autowired
    private HoldingRepository holdingRepository;

    @Override
    public Collection<Holding> getHoldings() {
        return holdingRepository.findAll();
    }

    @Override
    public Collection<Holding> getHoldings(HoldingType holdingType) {
        return holdingRepository.findByHoldingType(holdingType);
    }

    @Override
    public void addHolding(Holding holding) {
        HoldingType holdingType = holding.getHoldingType();
        if (holdingType == HoldingType.CASH) {
            Collection<Holding> holdings = holdingRepository.findByHoldingType(HoldingType.CASH);
            Holding cashHolding;
            if (holdings.iterator().hasNext()) {
                cashHolding = holdings.iterator().next();
                cashHolding.setAmount(cashHolding.getAmount() + holding.getAmount());
            } else {
                cashHolding = holding;
            }
            holdingRepository.save(cashHolding);
        } else {
            holdingRepository.insert(holding);
        }
    }


    @Override
    public void removeHolding(ObjectId id) {
        holdingRepository.deleteById(id);
    }

    @Override
    public double getBalance() {
        Collection<Holding> holdings = holdingRepository.findByHoldingType(HoldingType.valueOf("CASH"));
        Holding cashHolding = holdings.iterator().next();
        return cashHolding.getAmount();
    }

    @Override
    public void updateBalance(double newBalance) {
        Collection<Holding> holdings = holdingRepository.findByHoldingType(HoldingType.valueOf("CASH"));
        Holding cashHolding = holdings.iterator().next();
        cashHolding.setAmount(newBalance);
        holdingRepository.save(cashHolding);
    }

}
