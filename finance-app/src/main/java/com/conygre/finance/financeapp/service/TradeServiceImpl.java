package com.conygre.finance.financeapp.service;

import java.util.Collection;
import java.util.Optional;

import com.conygre.finance.financeapp.entities.Trade;
import com.conygre.finance.financeapp.entities.Trade.TradeState;
import com.conygre.finance.financeapp.repo.TradeRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeServiceImpl implements TradeService {

    @Autowired
    private TradeRepository tradeRepository;

    @Override
    public Collection<Trade> getTrades() {
        return tradeRepository.findAll();
    }

    @Override
    public void addTrade(Trade trade) {
        tradeRepository.insert(trade);
    }

    @Override
    public void removeTrade(ObjectId id) {
        tradeRepository.deleteById(id);
    }

    @Override
    public boolean updateTrade(Trade trade) {
        if (trade.verify()) {
            Trade prevTrade = tradeRepository.findById(trade.getId()).get();
            if (prevTrade.getTradeState() == TradeState.CREATED) {
                tradeRepository.save(trade);
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    @Override
    public Trade getTrade(ObjectId id) {
        Optional<Trade> trade = tradeRepository.findById(id);
        return trade.isPresent() ? trade.get() : null;
    }
}