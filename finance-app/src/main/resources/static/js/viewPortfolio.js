window.onload = async (e) => {
    e.preventDefault();
    const form = document.querySelector("#getTradesForm");
  
    const response = await fetch("http://localhost:8090/holdings", {
      method: "GET",
    });
  
    const holdings = await response.json();
    const table = document.createElement("table");
    table.setAttribute('class', 'history');
    const fields = ["name", "holdingType", "price", "amount"];
    const headerRow = table.insertRow();
  
    // Add header row
    fields.forEach((field) => {
      const headerCell = document.createElement("th");
      headerCell.innerHTML = field;
      headerRow.appendChild(headerCell);
    });
  
    // Add data rows
    holdings.forEach((trade) => {
      const row = table.insertRow();
      fields.forEach((field) => {
        const cell = row.insertCell();
        cell.innerHTML = trade[field];
      });
    });
  
    document.querySelector("#response").appendChild(table);
}