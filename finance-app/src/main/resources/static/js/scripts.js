
buyStockForm.onsubmit = async (e) => {
  e.preventDefault();
  const form = document.querySelector("#buyStockForm");
  let stockTicker = form.querySelector('input[name="ticker"]').value;
  let price = await getPrice(stockTicker);
  let stockQuantity = form.querySelector('input[name="quantity"]').value;
  const data = {
    date: new Date().toISOString(),
    ticker: stockTicker,
    quantity: stockQuantity,
    requestedPrice: price, 
    tradeState: "CREATED",
  };

  const response = await fetch("http://localhost:8090/trades", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });

  const text = await response.text(); // read response body as text
  document.querySelector("#response").innerHTML = text;
};

addCash.onsubmit = async (e) => {
  e.preventDefault();
  const form = document.querySelector("#addCash");
  let amount = form.querySelector('input[name="amount"]').value;
  
  const data = {
    holdingType: "CASH",
    amount: amount
  };

  const response = await fetch("http://localhost:8090/holdings", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });

  const text = await response.text();
  document.querySelector("#response").innerHTML = text;
};

 async function getPrice(ticker) {
  let url = 'http://127.0.0.1:5000/price/' + ticker;
  let price;
  let response = await fetch(url);
  let json = await response.json();
  price = json.slice(-1)[0].Open
  
  return price;
}




