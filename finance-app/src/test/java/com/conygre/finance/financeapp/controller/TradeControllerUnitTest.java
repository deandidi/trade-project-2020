package com.conygre.finance.financeapp.controller;

import com.conygre.finance.financeapp.FinanceAppApplication;
import com.conygre.finance.financeapp.controllers.TradeController;
import com.conygre.finance.financeapp.entities.Trade;
import com.conygre.finance.financeapp.repo.TradeRepository;
import com.conygre.finance.financeapp.service.TradeService;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.is;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = FinanceAppApplication.class)
public class TradeControllerUnitTest {

    public static final String TEST_ID = "5f496b84e1f97666710c9705";

    @Configuration
    public static class Config {
        @Bean
        public TradeRepository tradeRepository() {
            return mock(TradeRepository.class);
        }

        @Bean
        public TradeService tradeService() {
            ObjectId id = new ObjectId(TEST_ID);
            Trade trade = new Trade();
            List<Trade> trades = new ArrayList<>();
            trades.add(trade);

            TradeService tradeService = mock(TradeService.class);
            when(tradeService.getTrades()).thenReturn(trades);
            when(tradeService.getTrade(id)).thenReturn(trade);
            return tradeService;
        }

        @Bean
        public TradeController tradeController() {
            return new TradeController();
        }
    }

    @Autowired
    private TradeController tradeController;

    @Test
    public void testFindAll() {
        Iterable<Trade> trades = tradeController.getTrades();
        Stream<Trade> stream = StreamSupport.stream(trades.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }

    @Test
    public void testTradeById() {
        Trade trade = tradeController.getTrade(TEST_ID);
        assertThat(trade, is(notNullValue()));
    }

}