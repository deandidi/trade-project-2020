package com.conygre.finance.financeapp.controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;

import java.util.List;

import com.conygre.finance.financeapp.entities.Trade;

import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

public class TradeControllerFunctionalTest {
    private RestTemplate template = new RestTemplate();

    @Test
    public void testFindAll() {
        @SuppressWarnings("unchecked") // specifying raw List.class
        List<Trade> trades = template.getForObject("http://localhost:8080/trades", List.class);

        assertThat(trades, hasSize(1));
    }

    @Test
    public void testTradeById() {
        Trade trade = template.getForObject
            ("http://localhost:8080/trades/5f496b84e1f97666710c9705", Trade.class);
        assertThat(trade, notNullValue());
    }
}