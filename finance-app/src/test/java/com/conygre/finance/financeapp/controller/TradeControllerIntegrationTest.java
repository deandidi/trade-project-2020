package com.conygre.finance.financeapp.controller;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.conygre.finance.financeapp.FinanceAppApplication;
import com.conygre.finance.financeapp.controllers.TradeController;
import com.conygre.finance.financeapp.entities.Trade;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = FinanceAppApplication.class)
public class TradeControllerIntegrationTest {

    public static final String TEST_ID = "5f496b84e1f97666710c9705";

    @Autowired
    private TradeController controller;

    @Test
    public void testFindAll() {
        Iterable<Trade> trades = controller.getTrades();
        Stream<Trade> stream = StreamSupport.stream(trades.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }

    @Test
    public void testTradeById() {
        Trade trade = controller.getTrade(TEST_ID);
        assertThat(trade, is(notNullValue()));
    }
}