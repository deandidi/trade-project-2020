PUT http://localhost:8080/trades
Content-Type: application/json

{
    "id" : "5f496b84e1f97666710c9705",
    "date" : "2020-08-28",
    "ticker" : "NVDA",
    "quantity" : 100,
    "requestedPrice" : 501.07,
    "tradeState": "PENDING"
}